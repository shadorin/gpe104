﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Transform : MonoBehaviour
{
    private Transform tf; // A variable to hold our Transform component
    Vector3 myVector = new Vector3(0, 0, 0);//Vector for starting postion 
    public float speed= 0.01f; //The variable to control speed set for desginers beginning set at 0.01

    // Start is called before the first frame update
    void Start()
    {
        //Get the Trasnform component
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        //Move right every frame draw at a speed determined by the designer
        transform.position += (Vector3.right * speed);
    }
}
